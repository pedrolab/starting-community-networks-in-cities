Starting, Contributing and Empowering Community Networks in cities
=========================

The present project studies the growth of a community network in
Barcelona. The work has developed in two scenarios. In the first one,
a community network has been grown in Sant Andreu, a neighbourhood
where no community network was available. In the second scenario, an
existing community network has been strengthened in Poblenou. This
second scenario contemplates the tunneling of traffic using academic
networks between two universities to complement community wireless
backbones.